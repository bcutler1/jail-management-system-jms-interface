import com.sustain.cases.model.Case
import com.sustain.cases.model.SubCase
import com.sustain.entities.custom.CtInterfaceTracking
import com.sustain.properties.model.SystemProperty

if(!SystemProperty.getValue(interfaces.global.enabled)?.equalsIgnoreCase(true)) throw new Exception(Interfaces are not enabled in system properties);
if(!SystemProperty.getValue(interfaces.jailmanagementsystem.enabled)?.equalsIgnoreCase(true)) throw new Exception(Jail Management System Interface is not enabled in system properties);
new JailManagementInterface(this).exec();

public class InterfaceTracking {
    CtInterfaceTracking _tracking;
    static final String INTERFACE_NAME = INT_JMS;
    static final String RESULT_SUCCESS = SUCCESS;
    static final String RESULT_WARNING = ERROR
    static final String RESULT_FAIL = FAIL;

    InterfaceTracking(Date execDate) {
        _tracking = new CtInterfaceTracking();
        _tracking.setExecutionDate(execDate);
        _tracking.setName(INTERFACE_NAME);
        _tracking.saveOrUpdate();
    }
    void setRecordsProcessed(int r) {
        _tracking.setBatchSize(r);
    }
    void setException(String e) {
        _tracking.setException(e?.size()>255?e.substring(0,255):e);
    }
    void setMemo(String m) {
        _tracking.setMemo(m);
    }
    void updateStatus(String status) {
        _tracking.setResult(status);
    }
    void updateInterfaceTracking(String status, String exception, String memo) {
        setException(exception?.size()>255?exception.substring(0,255):exception);
        setMemo(memo);
        updateStatus(status)
        _tracking.saveOrUpdate();
    }
}

class JailManagementInterface {

    Script rule_;
    String emailRecipients_;
    String jmsUrl_;
    String jmsFilename_;
    List<String> errors_;

    InterfaceTracking interfaceTracking_;

    JailManagementInterface(def rule) {
        rule_ = rule;
        errors_ = new ArrayList<String>();
    }

    void exec() {
        Date execDate = new Date();
        try {
            interfaceTracking_ = new InterfaceTracking(execDate);
            setSystemParameters();

            Case cse = Case.get(123L); //todo:  determine how to pass in or query Case object

            if (cse != null) {
                Incidents incidents = getJson(cse);

                if (incidents != null) {
                    processJson(incidents);
                } else {
                    
                }
            } else {

            }
        }
        catch(Exception ex) {
            rule_.logger.info(ex.toString());
            interfaceTracking_.updateInterfaceTracking(interfaceTracking_.RESULT_FAIL, ex.toString(), CasesFailed: $casesFailed_ CasesUpdated: $casesUpdated_ CasesCreated: $casesCreated_)
            sendEmail(ex.toString())
            throw ex;
        }
    }

    void setSystemParameters() {
        if(SystemProperty.getValue(interfaces.jailmanagementsystem.emailRecipients)?.equalsIgnoreCase()) throw new Exception(System Property interfaces.jailmanagementsystem.emailRecipients cannot be blank);
        if(SystemProperty.getValue(interfaces.jailmanagementsystem.jmsUrl)?.equalsIgnoreCase()) throw new Exception(System Property interfaces.jailmanagementsystem.jmsUrl cannot be blank);

        emailRecipients_ = SystemProperty.getValue(interfaces.jailmanagementsystem.emailRecipients);
        jmsUrl_ = SystemProperty.getValue(interfaces.jailmanagementsystem.jmsUrl);
    }

    Incidents getJson(Case cse) {

        if (cse != null) {
            Incident incident_ = new Incident();

            SubCase sc_ = cse.subCase;


            incident_.setIncidentNumber(cse.subCase.parties.charges.)


            incident_.setBookingDateTime()


            Incidents incidents_ = new Incidents();
            incidents_.getIncidents().add(incident_);

            return incidents_;
        } else {
            return null;
        }
    }

    void processJson(Incidents incidents) {

        URL url = new URL(jmsUrl_);
        HttpURLConnection postConnection = (HttpURLConnection) url.openConnection();
        postConnection.setRequestMethod(POST);
        postConnection.setRequestProperty(Content-Type,application/json);
        postConnection.setDoOutput(true);
        OutputStream outputStream = postConnection.getOutputStream();
        outputStream.write(Incidents.getBytes());
        outputStream.flush();
        outputStream.close();

        int responseCode = postConnection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {

        } else {

        }
    }

    class Incidents implements Serializable {

        private List<Incident> incidents_;

        public Incident() {
            incidents_ = new ArrayList<>();
        }

        List<Incident> getIncidents() { return incidents_; }
        void setIncidents(List<Incident> incidents) { incidents_ = incidents; }
    }

    class Incident implements Serializable {

        private String incidentNumber_ = "";
        private String arrestDate_ = "";
        private String arrestingOfficerName_ = "";
        private String arrestingOfficerID_ = "";
        private String arrestingOfficerDiv_ = "";
        private String arrestingOfficerAgency_ = "";
        private String backingOfficerName_ = "";
        private String backingOfficerID_ = "";
        private String backingOfficerDiv_ = "";
        private String backingOfficerAgency_ = "";
        private String bookingDateTime_ = "";
        private String courtDate_ = "";
        private String detaineeIDNumber_ = "";
        private String courtDivision_ = "";
        private String firstName_ = "";
        private String middleName_ = "";
        private String lastName_ = "";
        private String dob_ = "";
        private String heightInches_ = "";
        private String weight_ = "";
        private String hairColor_ = "";
        private String eyeColor_ = "";
        private String skinColor_ = "";
        private String race_ = "";
        private String gender_ = "";
        private String personalOddities_ = "";
        private String tattoos_ = "";
        private String warningIndicators_ = "";
        private String spokenLanguage_ = "";
        private List<Charge> charges_ = "";

        public Incident() {
            charges_ = new ArrayList<>();
        }

        String getIncidentNumber() { return incidentNumber_; }
        void setIncidentNumber(String incidentNumber) { this.incidentNumber_ = incidentNumber_; }

        String getArrestDate() { return arrestDate_; }
        void setArrestDate(String arrestDate) { this.arrestDate_ = arrestDate_; }

        String getArrestingOfficerName() { return arrestingOfficerName_; }
        void setArrestingOfficerName(String arrestingOfficerName) { this.arrestingOfficerName_ = arrestingOfficerName_; }

        String getArrestingOfficerID() { return arrestingOfficerID_; }
        void setArrestingOfficerID(String arrestingOfficerID) { this.arrestingOfficerID_ = arrestingOfficerID_; }

        String getArrestingOfficerDiv() { return arrestingOfficerDiv_; }
        void setArrestingOfficerDiv(String arrestingOfficerDiv) { this.arrestingOfficerDiv_ = arrestingOfficerDiv_; }

        String getArrestingOfficerAgency() { return arrestingOfficerAgency_; }
        void setArrestingOfficerAgency(String arrestingOfficerAgency) { this.arrestingOfficerAgency_ = arrestingOfficerAgency_; }
        
        String getBackingOfficerName() { return backingOfficerName_; }
        void setBackingOfficerName(String backingOfficerName) { this.backingOfficerName_ = backingOfficerName_; }

        String getBackingOfficerID() { return backingOfficerID_; }
        void setBackingOfficerID(String backingOfficerID) { this.backingOfficerID_ = backingOfficerID_; }

        String getBackingOfficerDiv() { return backingOfficerDiv_; }
        void setBackingOfficerDiv(String backingOfficerDiv) { this.backingOfficerDiv_ = backingOfficerDiv_; }

        String getBackingOfficerAgency() { return backingOfficerAgency_; }
        void setBackingOfficerAgency(String backingOfficerAgency) { this.backingOfficerAgency_ = backingOfficerAgency_; }

        String getBookingDateTime() { return bookingDateTime_; }
        void setBookingDateTime(String bookingDateTime) { this.bookingDateTime_ = bookingDateTime_; }
        
        String getCourtDate() { return courtDate_; }
        void setCourtDate(String courtDate) { this.courtDate_ = courtDate_; }

        String getDetaineeIDNumber() { return detaineeIDNumber_; }
        void setDetaineeIDNumber(String detaineeIDNumber) { this.detaineeIDNumber_ = detaineeIDNumber_; }

        String getCourtDivision() { return courtDivision_; }
        void setCourtDivision(String courtDivision) { this.courtDivision_ = courtDivision_; }

        String getFirstName() { return firstName_; }
        void setFirstName(String firstName) { this.firstName_ = firstName_; }

        String getMiddleName() { return middleName_; }
        void setMiddleName(String middleName) { this.middleName_ = middleName_; }

        String getLastName() { return lastName_; }
        void setLastName(String lastName) { this.lastName_ = lastName_; }

        String getDob() { return dob_; }
        void setDob(String dob) { this.dob_ = dob_; }

        String getHeightInches() { return heightInches_; }
        void setHeightInches(String heightInches) { this.heightInches_ = heightInches_; }

        String getWeight() { return weight_; }
        void setWeight(String weight) { this.weight_ = weight_; }

        String getHairColor() { return hairColor_; }
        void setHairColor(String hairColor) { this.hairColor_ = hairColor_; }
        
        String getEyeColor() { return eyeColor_; }
        void setEyeColor(String eyeColor) { this.eyeColor_ = eyeColor_; }
        
        String getSkinColor() { return skinColor_; }
        void setSkinColor(String skinColor) { this.skinColor_ = skinColor_; }
        
        String getRace() { return race_; }
        void setRace(String race) { this.race_ = race_; }
        
        String getGender() { return gender_; }
        void setGender(String gender) { this.gender_ = gender_; }

        String getPersonalOddities() { return personalOddities_; }
        void setPersonalOddities(String personalOddities) { this.personalOddities_ = personalOddities_; }

        String getTattoos() { return tattoos_; }
        void setTattoos(String tattoos) { this.tattoos_ = tattoos_; }

        String getWarningIndicators() { return warningIndicators_; }
        void setWarningIndicators(String warningIndicators) { this.warningIndicators_ = warningIndicators_; }

        String getSpokenLanguage() { return spokenLanguage_; }
        void setSpokenLanguage(String spokenLanguage) { this.spokenLanguage_ = spokenLanguage_; }

        List<Charge> getCharges() { return charges_; }
        void setCharges(List<Charge> charges) { this.charges_ = charges; }
    }

    class Charge implements Serializable {

        private String jurisdiction_ = "";
        private String docketID_ = "";
        private String crimeDescrip_ = "";
        private String title_ = "";
        private String section_ = "";
        private String paragraph_ = "";
        private String offenseDate_ = "";
        private String warrantNumber_ = "";
        private String bond_ = "";
        
        public Charge() { }

        String getJurisdiction() { return jurisdiction_; }
        void setJurisdiction_(String jurisdiction) { this.jurisdiction_ = jurisdiction; }

        String getDocketID() { return docketID_; }
        void setDocketID_(String docketID) { this.docketID_ = docketID; }

        String getCrimeDescrip() { return crimeDescrip_; }
        void setCrimeDescrip_(String crimeDescrip) { this.crimeDescrip_ = crimeDescrip; }

        String getTitle() { return title_; }
        void setTitle_(String title) { this.title_ = title; }
        
        String getSection() { return section_; }
        void setSection_(String section) { this.section_ = section; }
        
        String getParagraph() { return paragraph_; }
        void setParagraph_(String paragraph) { this.paragraph_ = paragraph; }

        String getOffenseDate() { return offenseDate_; }
        void setOffenseDate_(String offenseDate) { this.offenseDate_ = offenseDate; }

        String getWarrantNumber() { return warrantNumber_; }
        void setWarrantNumber_(String warrantNumber) { this.warrantNumber_ = warrantNumber; }

        String getBond() { return bond_; }
        void setBond_(String bond) { this.bond_ = bond; }
    }
}

//        JMS Field Name	R,O	eCourt Field Name	eCourt Entity	eCourt Field Name	eCourt Field Path	Data Type	Default Value	Notes
//        Incident Panel
//        IncidentNumber	required	Case Number	Charge	ticketNumber	case.subCases.parties.charges.ticketNumber	String
//        ArrestDate	required	Arrest Date	arrests	arrestDate	case.subCases.parties.arrests.arrestDate	Date
//        ArrestingOfficerName		Last Name	person	lastName	case.subCases.assignments.person.lastName	String		The ArrestingOfficerName will be comprised of these fields
//        First Name	person	firstName	case.subCases.assignments.person.firstName	String
//        Middle Name	person	middleName	case.subCases.assignments.person.middleName	String
//        ArrestingOfficerID		Officer ID	identification	identificationNumber	case.subCases.assignments.person.identifications.identificationNumber	String
//        ArrestingOfficerDivision
//        ArrestingOfficerAgency		Officer Agency	person	organizationName	case.subCases.assignments.person.organizationName	String
//        BackingOfficerName
//        BackingOfficerID
//        BackingOfficerDivision
//        BackingOfficerAgency
//        Booking Date Time	required	Booking Date	arrests	arrestDate	case.arrestDate	Date		The BookingDateTime will be comprised of these fields
//        Booking Time	arrests	arrestTime	case.arrestTime	Date
//        CourtDate		Appear Date	scheduledEvent	startDateTime	case.hearings.startDateTime	Date
//        Detainee ID Number	required	Arrest Number	arrests	bookingNumber	case.subCases.parties.arrests.bookingNumber	String
//        CourtDivision		Court Division	ScheduledEvent	eventLocation	case.hearings.eventLocation	Lookup List
//        FirstName	required	First Name	person	firstName	case.subCases.parties.person.firstName	String
//        MiddleName	required	Middle Name	person	middleName	case.subCases.parties.person.middleName	String
//        LastName	required	Last Name	person	lastName	case.subCases.parties.person.lastName	String
//        DOB	required	Date of Birth	personProfile	dateOfBirth	case.subCases.parties.person.profiles.dateOfBirth	Date
//        HeightInches		Height	personProfile	height	case.subCases.parties.person.profiles.height	String
//        Weight		Weight	personProfile	weight	case.subCases.parties.person.profiles.weight	String
//        HairColor		Hair Color	personProfile	hairColor	case.subCases.parties.person.profiles.hairColor	Lookup List
//        EyeColor		Eye Color	personProfile	eyeColor	case.subCases.parties.person.profiles.eyeColor	Lookup List
//        SkinColor
//        Race		Race	personProfile	ethnicity	case.subCases.parties.person.profiles.ethnicity	Lookup List
//        Gender		Gender	personProfile	gender	case.subCases.parties.person.profiles.gender	Lookup List
//        PersonalOddities
//        Tattoos
//        WarningIndicators
//        SpokenLanguage		Primary Language	personProfile	primaryLanguage	case.subCases.parties.person.profiles.primaryLanguage	Lookup List
//        Charges Panel (this panel is repeatable)
//        Charge		Violation	charge	statute	case.subCases.parties.charges.statute	String
//        Jurisdiction		Case Jurisdiction	Case	caseType	case.caseType	String
//        DocketID	required	Case Number	Charge	ticketNumber	case.subCases.parties.charges.ticketNumber	String
//        CrimeDescrip
//        Title	required	Violation	charge	statute	case.subCases.parties.charges.statute	String
//        Section	required
//        Paragraph	required
//        OffenseDate	required	Violation Date	charge	chargeDate	case.subCases.parties.charges.chargeDate	date
//        WarrantNumber		Warrant Number	warrant	trackingNumber	warrant.trackingNumber	String
//        Bond		Bond Number	bail	bondNumber	bail.bondNumber	String
